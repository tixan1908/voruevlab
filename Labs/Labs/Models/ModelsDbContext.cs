﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labs.Models
{
    public class ModelsDbContext: DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Photo> Photos { get; set; }

        public ModelsDbContext(DbContextOptions<ModelsDbContext> options) : base(options) {
            Database.EnsureCreated();
        }
    }
}
