﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Labs.Models.ClientModels
{
    public class ItemClient
    {
        [Required]
        public string Name { get; set; }
        
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public decimal Volume { get; set; }
        public int? PhotoId { get; set; }
    }
}
