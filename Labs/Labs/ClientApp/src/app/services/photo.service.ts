import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {PhotoModel} from '../models/PhotoModel';

@Injectable()
export class PhotoService {

    constructor( private apiService: ApiService) {}

    public uploadPhoto(formData: FormData): Observable<PhotoModel> {
        const headers = new Headers();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        return this.apiService.post('photo/create', formData, { headers: headers});
    }
}
