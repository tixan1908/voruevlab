import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {ItemModel} from '../models/ItemModel';

@Injectable()
export class ItemService {
    constructor( private apiService: ApiService) {}

    public getList(): Observable<Array<ItemModel>> {
        return this.apiService.get('sampleData/getList');
    }

    public create(form): Observable<any> {
        return this.apiService.post('sampleData/create', form);
    }
}
