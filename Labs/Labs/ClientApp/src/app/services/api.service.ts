import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {
    constructor (private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

    public get(path: string, options?: any): Observable<any> {
        return this.http.get(`${this.baseUrl}api/${path}`, options);
    }

    public post(path: string, body?: any, options?: any): Observable<any> {
        return this.http.post(`${this.baseUrl}api/${path}`, body, options);
    }

    public put(path: string, body?: any, options?: any): Observable<any> {
        return this.http.put(`${this.baseUrl}api/${path}`, body, options);
    }

    public delete(path: string, options?: any): Observable<any> {
        return this.http.delete(`${this.baseUrl}api/${path}`, options);
    }
}
