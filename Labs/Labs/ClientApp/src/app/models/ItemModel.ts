import {PhotoModel} from './PhotoModel';

export class ItemModel {
    public id: number;
    public name: string;
    public description: string;
    public cost: number;
    public volume: number;
    public photo: PhotoModel;
}
