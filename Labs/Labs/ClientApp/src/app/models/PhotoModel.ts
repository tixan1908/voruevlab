export class PhotoModel {
    public id: number;
    public name: string;
    public path: string;
}
