import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ItemService} from '../../services/item.service';
import {PhotoService} from '../../services/photo.service';
import {PhotoModel} from '../../models/PhotoModel';
import {Router} from '@angular/router';

@Component({
    selector: 'app-create-item',
    templateUrl: './create-item.component.html',
    styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {

    public form: FormGroup;

    public uploadPhoto: string;

    @ViewChild('imagefile') imageFile: ElementRef;

    constructor (
        private formBuilder: FormBuilder,
        private itemService: ItemService,
        private photoService: PhotoService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.form = this.formBuilder.group({
           name: [null, [Validators.required]],
           description: [null],
           cost: [null, [Validators.required]],
           volume: [null, [Validators.required]],
           photoId: [null, [Validators.required]]
        });
    }

    public onSubmit(): void {
        this.itemService.create(this.form.value).subscribe(
            (resp) => {
                this.router.navigateByUrl('/');
            },
            (err) => {
                //  handle error
            }
        );
    }


    public fileChangeListener(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            this.photoService.uploadPhoto(formData).subscribe(
                (resp: PhotoModel) => {
                    this.uploadPhoto = resp.path;
                    this.form.controls.photoId.setValue(resp.id);
                },
                (err) => {
                    //  handle error
                }
            );
        }
    }

    public handleClickUploadPhoto() {
        this.imageFile.nativeElement.click();
    }
}
