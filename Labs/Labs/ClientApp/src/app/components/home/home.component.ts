import {Component, OnInit} from '@angular/core';
import {ItemService} from '../../services/item.service';
import {ItemModel} from '../../models/ItemModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public list: Array<ItemModel> = [];

  constructor(private itemService: ItemService) {}

  ngOnInit(): void {
    this.itemService.getList().subscribe(
        (list: Array<ItemModel>) => {
          this.list = list;
        },
        (err) => {
          //  handle error
        }
    );
  }
}
