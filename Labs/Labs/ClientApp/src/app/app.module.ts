import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './components/home/home.component';
import {ApiService} from './services/api.service';
import {ItemService} from './services/item.service';
import {CreateItemComponent} from './components/create-item/create-item.component';
import {PhotoService} from './services/photo.service';

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'create', component: CreateItemComponent },
    ])
  ],
  providers: [ApiService, ItemService, PhotoService],
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CreateItemComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
