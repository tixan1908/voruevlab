using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Labs.Models;
using Labs.Models.ClientModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Labs.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {

        public ModelsDbContext db;

        public SampleDataController(ModelsDbContext context) { db = context; }


        [HttpGet("[action]")]
        public IEnumerable<Item> GetList()
        {
            return db.Items.Include(x => x.Photo).ToList();
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetById(int id)
        {
            return Json(await db.Items.FirstOrDefaultAsync(val => val.Id == id));
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Create([FromBody] ItemClient item)
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(item);

            if (Validator.TryValidateObject(item, context, results, true))
            {
                Photo photo = null;
                if (item.PhotoId != null)
                {
                    photo = await db.Photos.FirstOrDefaultAsync(val => val.Id == item.PhotoId);
                }

                var newItem = new Item
                {
                    Name = item.Name,
                    Cost = item.Cost,
                    Photo = photo,
                    Description = item.Description,
                    Volume = item.Volume
                };

                await db.AddAsync(newItem);
                await db.SaveChangesAsync();
                return Json(newItem.Id);
            }

            return BadRequest();

        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id >= 0)
            {
                var item = await db.Items.FirstOrDefaultAsync(val => val.Id == id);
                db.RemoveRange(item);
                await db.SaveChangesAsync();
                return Json("Succesfuly deleted");
            }

            return BadRequest();
        }
    }
}
