﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Labs.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Labs.Controllers
{
    [Route("api/[controller]")]
    public class PhotoController : Controller
    {
        private ModelsDbContext db;
        private IHostingEnvironment appEnvironment;
        private IConfiguration config;

        public PhotoController(ModelsDbContext db, IHostingEnvironment appEnvironment, IConfiguration config)
        {
            this.db = db;
            this.appEnvironment = appEnvironment;
            this.config = config;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Create(IFormFile uploadFile)
        {
            if (uploadFile != null)
            {
                string path = "/Files/" + uploadFile.FileName;
                
                using (var fileStream = new FileStream(appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadFile.CopyToAsync(fileStream);
                }

                var newPath = config.GetValue<string>("Urls:BaseUrl") + path;

                Photo file = new Photo { Name = uploadFile.FileName, Path = newPath };

                await db.Photos.AddAsync(file);
                await db.SaveChangesAsync();

                return Json(file);
            }

            return BadRequest();
        }

        [HttpGet("action")]
        public async Task<IActionResult> Get(int id)
        {
            if (id >= 0)
            {
                return Json(await db.Photos.FirstOrDefaultAsync(val => val.Id == id));
            }

            return BadRequest();
        }
    }
}